<?php

abstract class banco
{
    private $tipodaconta;
    public function gettipodaconta(){
        return $this-> tipodaconta;
    }
    public function settipodaconta(string $tipodaconta){
        $this-> tipodaconta = $tipodaconta;
    }
    public string $agencia = " ";
    public string $conta = " ";
    public float $saldo =  0.0;

    public function imprimeExtrato()
    {
        echo "Conta " . $this->tipodaconta . "Agência: " . $this->agencia . " conta: " . $this->conta . "saldo: " . $this->calculaSaldo();
    }

     public function deposito($valorDepositado)
     {
         if($valorDepositado > 0 )
         //$this-> saldo += $valorDepositado;
         $this-> saldo += $valorDepositado;
         echo "Depósito efetuado com sucesso!!!" .'<br>';

     }

    public function saque($valorSaqueado)
    {
        $this-> saldo -= $valorSaqueado;
        {
                if($this->saldo >= $valorSaqueado){
                $this->saldo -= $valorSaqueado;
                echo "Saque realizado com sucesso";
    }else
    {
                echo "Saldo insuficiente";
            }
            
        }
    }

    abstract public function calculaSaldo();
}

 class poupanca extends banco
 {
     public $reajuste;

     public function __construct(string $agencia, string $conta, float $reajuste)
     {
         $this-> setipoDaconta = ('Poupança');
         $this-> agencia = $agencia;
         $this-> conta = $conta;
         $this-> reajuste = $reajuste;
     }

     public function calculaSaldo()
     {
         return $this->saldo + ($this->saldo * $this->reajuste/100);
     }
 }

class Especial extends banco
{
    public $saldoEspecial;

    public function __construct(string $agencia, string $conta, string $saldoEspecial)
    {
        $this-> tipodaconta = 'Especial';
        $this-> agencia = $agencia;
        $this-> conta = $conta;
        $this-> saldoEspecial = $saldoEspecial;
    }
    public function calculaSaldo()
    {
        return $this->saldo + $this->saldoEspecial;
    }
}

$ctaPoup = new Poupanca( '0002-7', '85588-88', 0.54);
//$ctaPoup-> saldo = -1500; //nao pode acessar atributo protegido
$ctaPoup-> deposito (1500);
$ctaPoup-> saque(300);
$ctaPoup-> imprimeExtrato();

echo '<br>';

$ctaEspecial = new Especial('0055-2', '75588-42', 2300);
$ctaEspecial-> deposito(1500);
$ctaEspecial-> imprimeExtrato();
