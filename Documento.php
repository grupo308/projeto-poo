<?php




     abstract class Documento
     {
         protected $numero;

         abstract public function eValido();

         abstract public function formata();

         public function setNumero($numero)
         {
             $this-> numero = preg_replace('/[^0-9]/', '', $numero); //expressão regular
         }

         public function getNumero()
         {
             return $this-> numero;
         }
     }

    class CPF extends Documento
    {
        public function __construct($numero)
        {
            $this-> setNumero($numero);
        }

        public function eValido()
        {
            $digitoX =0;
            $somatoriox = 0;
            $cpfx = substr($this-> getNumero(), 0, 9);
            $peso = 10;

            /* $somatoriox = $somatoriox = ($cpfx[0] * 10);
             $somatoriox = $somatoriox = ($cpfx[1] * 9);
             $somatoriox = $somatoriox = ($cpfx[2] * 8);
             $somatoriox = $somatoriox = ($cpfx[3] * 7);
             $somatoriox = $somatoriox = ($cpfx[4] * 6);
             $somatoriox = $somatoriox = ($cpfx[5] * 5);
             $somatoriox = $somatoriox = ($cpfx[6] * 4);
             $somatoriox = $somatoriox = ($cpfx[7] * 3);
             $somatoriox = $somatoriox = ($cpfx[8] * 2);*/
            for ($index = 0; $index <9; $index ++) {
                $somatoriox += $cpfx[$index] * $peso;
                //$peso= $peso - 1;
                $peso--;
            }
            echo $somatoriox;

            $modulo11  = $somatoriox% 11;

            echo "<br>" . $modulo11;



            $digitoX= 11 -$modulo11;

            echo "<br>" . $digitoX;

            if ($digitoX >9) {
                $digitoX =9;
            }

            echo "<br>" . $digitoX;
        }
        public function formata()
        {
            // Formato do CPF: ###.###.###-##
            return substr($this-> numero, 0, 3) . '.' .
                   substr($this-> numero, 3, 3) . '.' .
                   substr($this-> numero, 6, 3) . '-' .
                   substr($this-> numero, 9, 2);
        }
    }

    class CNPJ extends Documento
    {
        public function __construct($numero)
        {
            $this->setNumero($numero);
        }

        public function eValido()
        {
        }

        public function formata()
        {
        }
    }

    class CNH extends Documento
    {
        private $categoria;

        public function __construct($numero, $categoria)
        {
            $this-> setNumero($numero);
            $this-> categoria = $categoria;
        }

        public function eValido()
        {
        }

        public function formata()
        {
        }

        public function setCategoria($categoria)
        {
            $this-> categoria = $categoria;
        }

        public function getCategoria()
        {
            return $this-> categoria;
        }
    }
    $cpfMatheus = new CPF("501.479.628-11");



     $cpfMatheus->eValido();



     /*
     $cpf = new CPF('115.858.758-88');
     echo $cpf-> formata();

     $cpf-> setNumero('CPF: 545.855.565-85');

     echo '</br>';
     echo $cpf-> getNumero();

     echo '</br>';

     $cnpjSenac = new CNPJ('03.709.814/0025-65');
     echo $cnpjSenac-> getNumero();

     echo '</br>';

     $cnhFulano = new CNH('1255656','AB');
     echo $cnhFulano-> getNumero() .' Cat.: ' . $cnhFulano-> getCategoria();
*/
